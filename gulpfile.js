var notify = require('gulp-notify'),
    requireDir = require('require-dir');

// Global variables
gulp = require('gulp');
gulpif  = require('gulp-if');
browserSync = require('browser-sync').create();
// livereload  = require('gulp-livereload');
isProduction = process.env.NODE_ENV == 'production';
path = {
    'src': './src',
    'dest': './dist'
};
onError = function (error) {
	console.log(error);
	notify().write(error.message);
	this.emit('end');
};

requireDir('./gulp/tasks');
