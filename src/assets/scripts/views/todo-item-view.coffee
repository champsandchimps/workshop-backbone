Hammer = require('hammerjs')

module.exports = class TodoItemView extends Backbone.View

	tagName: 'li'

	initialize: =>
		this.el.innerHTML = this.model.get('description')

		this.removeButton = document.createElement('button')
		this.removeButton.classList.add('remove')
		this.removeButton.innerHTML = 'Remove'
		this.el.appendChild(this.removeButton)

		this.stateButton = document.createElement('button')
		this.stateButton.classList.add('state')
		this.stateButton.innerHTML = 'Completed'
		this.el.appendChild(this.stateButton)

		this.model.on('remove', this.onRemoveModel)
		this.model.on('change:completed', this.onModelCompleted)
		

		Hammer(this.el).on('swiperight', this.onSwipeRight)
		Hammer(this.el).on('swipeleft', this.onSwipeLeft)

	events: 
		'click button.state': 'onClickStateButton'
		'click button.remove': 'onClickRemoveButton'

	onSwipeLeft: =>
		this.model.collection.remove(this.model)

	onSwipeRight: =>
		this.model.toggleCompleted()

	onClickRemoveButton: =>
		this.model.collection.remove(this.model)
	
	onClickStateButton: =>
		this.model.toggleCompleted()

	onModelCompleted: =>
		this.el.classList.toggle('completed')

	onRemoveModel: =>
		this.el.parentNode.removeChild(this.el)