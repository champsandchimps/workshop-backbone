Request = require('browser-request')

module.exports = class InputView extends Backbone.View

	initialize: =>
		

	events: 
		'keyup': 'onKeyUp'
		
	onKeyUp: (event) =>
		switch event.keyCode
			when 13
				this.search()

	search: =>
		Request({method:'POST', url:'/db', body:'{"relaxed":true}', json:true}, this.onPost)

	onPost: (response) =>
		console.log(response)

		this.trigger('SEARCH')
		this.el.value = ''
		this.el.blur()

	value: =>
		return this.el.value


