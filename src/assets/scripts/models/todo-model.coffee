module.exports = class TodoModel extends Backbone.Model

	defaults:
		completed: false

	initialize: =>

	toggleCompleted: =>
		this.set('completed', !this.get('completed'))