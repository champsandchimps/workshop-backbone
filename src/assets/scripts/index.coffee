window.Backbone = require('backbone')
window.$ = require('jquery')
window._ = require('lodash')

# console.log(require('hammerjs')) 


TodoCollection = require('./models/todo-collection')
InputView = require('./views/input-view')
TodoListView = require('./views/todo-list-view')

class Main extends Backbone.View

	el: 'body'

	initialize: =>
		this.collection = new TodoCollection()

		this.inputView = new InputView(el: 'input')
		this.inputView.on('SEARCH', this.onSearch)

		this.todoView = new TodoListView(el: 'ul', this.collection)

		this.collection.set([{description: 'Förstör härskarringen'}, {description: 'Svik frodo'}])

	onSearch: =>
		this.collection.add(description: this.inputView.value())

new Main()
